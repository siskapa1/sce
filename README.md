# SCE - Detekce síťových anomálií

# Úvod

Projekt se zabývá detekcí anomálií v síťovém provozu s cílem určit podežrelé 
chování na síti. Na základě historických dat popisujících strukturu síťového provozu 
se vytváří model, který predikuje strukturu síťového provozu v následujících hodinách. Predikovaná 
data jsou následně porovnána s reálnou strukturou provozu a v případě 
výraznější odchylky reálného provozu od predikovaného je vygenerován report, upozorňující na detekovanou anomálii.

# Datová sada

Datovou sadu vytváří systém [NEMEA][1], který poskytuje data o struktuře síťového provozu v následujícím formátu:

```csv
uint64 BYTES,time TIME_FIRST,time TIME_LAST,uint32 COUNT,uint32 PACKETS,uint32 PREFIX_TAG,uint8 PROTOCOL
63235,2020-12-30T18:01:06.852,2020-12-30T18:06:17.472,505,816,1,6
127550,2020-12-30T18:01:17.237,2020-12-30T18:06:17.937,476,1718,1,17
...
```

Položka `BYTES`, `PACKETS` a `COUNT` obsahuje sumu bajtů, paketů a počet viděných 
flow vztahující se k cílové podsíti, která je reprezentována položkou `PREFIX_TAG`, a to
v časovém rozmezí zadaném položkami `TIME_FIRST` a `TIME_LAST`. 
Data jsou zachytávána v 5 minutových intervalech, nicméně pro jednu cílovou síť reprezentovanou položkou `PREFIX_TAG` může být v tomto rozmezí zachyceno více záznamů.

Testovací datová sada byla zachytávána pro jednu podsíť s prefixem /16, která reprezentovala 
celou síť jedné české vysoké školy, a to v období 9.12.2020 - 8.1.2021. Datová sada je dostupná zde `data/test_data.csv`

# Agregátor

Vzhledem k tomu, že data mohou obsahovat pro stejné časové rozmezí a stejnou cílovou 
síť více záznamů, je potřeba tato data před dalším zpracováním nejprve agregovat. K tomu slouží nástroj `Aggregator`, který agreguje 
data podle cílové podsítě v 5 minutových intervalech. Výstupní formát dat je stejný jako původní datová sada, ale již neobsahuje pro stejné časové rozmezí více záznamů. 

``` bash
usage: aggregator.py [-h] data_file

Aggregate historical network data.

positional arguments:
  data_file      Path to data file

optional arguments:
  -h, --help     show this help message and exit
```

# Predikce

Pro vytváření predikce/předpovědi struktury síťového provozu slouží nástroj `forecast.py`. Vstupem tohoto nástroje 
jsou agregovaná data z agregátoru, která jsou upravena do požadovaného formátu použité predikční knihovny. Pro predikci 
struktury síťových dat se používá knihovna [Facebook Prophet][2] a celkem je vytvářeno 6 predikcí. Predikce se vytváří pro
protokol UDP a TCP a ke každému protokolu se predikuje objem provozu v paketech, bajtech a počtu flow. 

Parametry predikční knihovny byly zvoleny na základě předchozích zkušeností a provedených experimentech tak, aby predikce co nejlěpe 
odpovídala reálnému provozu. Vstupem predikce byly zvoleny 5 minutové intervaly, které poskytují více vstupních hodnot (bodů) a tím 
pádem je možné provádět jemnější predikci. Výstupem predikčního algoritmu je předpověď struktury síťového provozu na následující 4 
hodiny v 5 minutových intervalech, celkem tedy 48 bodů. Pro každý bod jsou predikovány 3 hodnoty:

*  **yhat**: Vyjadřuje predikovanou hodnotu.
*  **yhat_lower** Vyjadřuje dolní mez predikované hodnoty. 
*  **yhat_upper** Vyjadřuje horní mez predikované hodnoty.

Výstupem nástroje je *json* soubor, který obsahuje predikované hodnoty v následujícím formátu:


```
{
  // Protocol identified by ip protocol number (by default `TCP`, `UDP`)
  "TCP": {
    // Metric - currently `bytes`, `packets` or `flow_count`
    "bytes": {
       // Start of the 5 minutes period in ISO8601 timestamp
      "2021-01-08T22:25:06.254000": {
        // Predicted value of metric
        "yhat": 3554708808.3982544,
        // Lower bound of predicted value of metric
        "yhat_lower": 1521118167.1637852,
        // Upper bound of predicted value of metric
        "yhat_upper": 5694236826.75895
      },
      ...
    },
    "packets": {
      "2021-01-08T22:25:06.254000": {
        "yhat": 3518491.759246801,
        "yhat_lower": 2043192.153646649,
        "yhat_upper": 5026790.98580869
      },
      ...
    },
    "flow_count": {
      "2021-01-08T22:25:06.254000": {
        "yhat": 600439.7973741421,
        "yhat_lower": 329078.64894653903,
        "yhat_upper": 867232.3304018258
      },
      ...
    },
  },
  ...
}
```

Použití nástroje:

-----

``` shell
usage: forecast.py [-h] data_file

Generate "prediction profile" based on aggregated historical network data.

positional arguments:
  data_file      Path to data file

optional arguments:
  -h, --help     show this help message and exit
```

# Detekce anomálií

Pro detekci anomálií v síťovém provozu slouží nástroj `anomaly_detector.py`, jehož vstupem 
jsou predikovaná data a reálná data, která byla naměřena v predikovaném čase na živé síti.

Vstupem nástroje je také prahová hodnota `threshold`, která vyjadřuje kolikrát musí být reálný objem 
provozu větší, než hodnota *yhat_upper*, udávající horní mez predikce, aby se tato událost vyhodnotila 
jako anomálie. Detekce anomálie se tak vztahuje pouze k predikované hodnotě *yhat_upper*, nicméně ostatní 
predikované hodnoty jsou také důležité, protože mohou sloužit k vizualizaci porovnání predikovaných a realých 
hodnot struktury provozu a zjištění, zda daná předpověď odpovídá realitě a tedy, že zvolený model funguje.

Na základě pozorování bylo zjištěno, že 5 minutový interval pro detekci anomálie je velmi krátká doba, protože 
v reálném provozu nastávají situace, kdy dochází vlivem nárazově zvýšené aktitivity uživatelů k odchylkám od 
predikované hodnoty, které ale nejsou anomálií (podežrelým chovaním) a bylo by tak generováno spoustu falešně 
pozitivních reportů. Model tyto odchylky správně nepredikuje, neboť bývají nárazové a z dlouhodobějšího hlediska 
nereprezentují strukturu provozu.

Proto byla zvolena varianta, kdy se 5 minutové predikce sdruží do hodinových a to tak, že se sečtou hodnoty z 
jednotlivých 5 minutových predikcí v rozmezí vždy jedné hodiny. V tomto časovém období se již nárazové zvýšení objemu provozu
typicky ztratí. Celkem tak je porovnáván skutečný provoz sítě s predikovaným ve 4 bodech, kde každý reprezentuje jednu hodinu.

Výstupem nástroje jsou grafy pro jednotlivé typy predikcí (pakety, bajty, flow) zobrazující porovnání predikových a skutečných 
hodnot a v případě detekované anomálie i textový výstup, který může vypadat například následovně:

````
Anomaly found in TCP-packets [2021-01-08 22:25:06.254000 - 2021-01-08 23:25:06.254000]: Real traffic is 4.21 times bigger than maximal predicted value.
````

Použití nástroje:

-----

``` shell
usage: anomaly_detector.py [-h] prediction reality threshold

Detect anomaly in real and predicted network data.

positional arguments:
  prediction      Path to file with predicted network data
  reality         Path to file with real network data
  threshold       Anomaly threshold

optional arguments:
  -h, --help     show this help message and exit
```

# Výsledky

Výsledky nástroje budou ukázány nad testovacími daty, bude tedy použita testovací datová sada
`data/test_data.csv`, která byla agregována a nad kterou byla následně provedena predikce. Tato predikce pak byla porovnána s 
reálným provozem `data/real_profile.csv`

Výsledky ukazují především správnost predikce síťového provozu vůči reálným naměřeným hodnotám. Pro 
jednoduchost bude ukázána predikce pouze pro protokol TCP.

První graf zobrazuje porovnání predikovaných hodnot (žlutá, modrá a červená čára) pro objem bajtů u
TCP protokolu s reálnou naměřenou hodnotou (zelená čára). Z grafu lze pozorovat, že predikce ve třech bodech ze čtyř
téměř přesně odpovídá reálně naměřeným hodnotám. U druhého bodu je reálná hodnota vyšší než predikovaná, nicméně 
je stále v predikovaném rozmezí.

![alt text](results/TCP-bytes.png)

Druhý graf ukazuje predikci pro objem paketů u protokolu TCP. Opět je vidět, že se reálně naměřené hodnoty přibližují 
predikovaným hodnotám.

![alt text](results/TCP-packets.png)

Poslední graf ukazuje porovnání predikce a reality pro počet viděných flow. Z graju lze pozorovat, že predikce byla 
mírně nadhodnocená, nicméně reálné hodnoty docela přesně kopírují hodnoty predikované s poměrně malou odchylkou.

![alt text](results/TCP-flow_count.png)

Nad testovacími daty nebyla nalezena anomálie, která by znamenala například proběhnutý DDoS útok. Proto zde bude pro ilustraci
ukázána umělě vytvořená anomálie a k ní vygenerovaný report. Anomálie byla do dat zanesena zvýšením objemu reálně 
naměřeného provozu. Výsledky se zanesenou anomálií ukazuje následující graf, na kterém lze pozorovat, že v druhém bodě, který 
reprezentuje čas mezi 15:40 - 16:40 došlo k výraznějšímu překročení reálně naměřených hodnot od maximální predikované hodnoty. Nástroj 
tuto anomálii správně detekoval a vypsal následující report:

````
Anomaly found in TCP-packets [2021-01-09 15:40:36.754000 - 2021-01-09 16:40:36.754000]: Real traffic is 1.44 times bigger than maximal predicted value.
````

![alt text](results/TCP-packets-anomaly.png)

Správnost predikce byla testována nad více instancemi testovacích dat, které zastupovaly rozdílné časy během dne (pracovní doba, noc) 
a rozdílné dny (pracovní den, víkend). Testy ukázaly správné nastavení parametrů predikce, která dostatečně přesně 
reprezentuje reálný provoz. Také se ověřila správná detekce anomálie a vypsání příslušného reportu. 

# Závěr

Výsledky implementovaného nástroje ukázují, že je možné nástroj použít k detekci anomálií v síťovém provozu. Dalším
pokračováním této práce bude případné rozšíření predikce o další protokoly nebo i konkrétní porty.


[1]: https://github.com/CESNET/Nemea
[2]: https://facebook.github.io/prophet/