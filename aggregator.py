#!/usr/bin/env python3
#
#   Copyright 2020 Pavel Siska <siskapa1@fit.cvut.cz>
#

import argparse
import datetime
from pathlib import Path
import array as arr
import pandas

PREFIX_TAG = 1
IP_PROTO_TRANS_TABLE = {
    6: 'TCP',
    17: 'UDP',
}

def load_data(data_file_path):
    data = pandas.read_csv(
        data_file_path,
        parse_dates=['time TIME_FIRST', 'time TIME_LAST'],
        infer_datetime_format=True,
        dtype={
            'uint64 BYTES': 'uint64',
            'uint32 COUNT': 'uint32',
            'uint32 PACKETS': 'uint32',
            'uint8 PREFIX_TAG': 'uint32',
            'uint8 PROTOCOL': 'uint8'
        })
    data.rename(
        columns={
            'uint64 BYTES': 'bytes',
            'time TIME_FIRST': 'time_first',
            'time TIME_LAST': 'time_last',
            'uint32 COUNT': 'flow_count',
            'uint32 PACKETS': 'packets',
            'uint32 PREFIX_TAG': 'prefix_tag',
            'uint8 PROTOCOL': 'protocol'
        },
        inplace=True)
    return data

def aggregate(path_file):
    data = load_data(path_file)

    print("time TIME_FIRST,uint64 BYTES,uint32 COUNT,uint32 PACKETS,uint8 PROTOCOL")

    for ip_proto_num, name in IP_PROTO_TRANS_TABLE.items():
        filtered_data = data.loc[(data['prefix_tag'] == PREFIX_TAG) & (data['protocol'] == ip_proto_num)]

        time_window = 0
        is_tw_set = False
        packets_sum = 0
        bytes_sum = 0
        flow_sum = 0;

        for index, row in filtered_data.iterrows():
            if is_tw_set == False:
                print(row['time_first'].isoformat(), end =",")
                time_window = row['time_first'] + datetime.timedelta(0,300)
                is_tw_set = True
            if row['time_first'] < time_window:
                packets_sum = packets_sum + row['packets']
                bytes_sum = bytes_sum + row['bytes']
                flow_sum = flow_sum + row['flow_count']
            else:
                print(bytes_sum, end =",")
                print(flow_sum, end =",")
                print(packets_sum, end =",")
                print(ip_proto_num)
                print(row['time_first'].isoformat(), end =",")
                time_window = row['time_first'] + datetime.timedelta(0,300)
                packets_sum = row['packets']
                bytes_sum = row['bytes']
                flow_sum = row['flow_count']
        print(bytes_sum, end =",")
        print(flow_sum, end =",")
        print(packets_sum, end =",")
        print(ip_proto_num)

def parse_args():
    """Parse arguments and return parsed options."""
    parser = argparse.ArgumentParser(
        description='Aggregate historical network data.'
    )
    parser.add_argument('data_file', help='Path to data file')
    return parser.parse_args()

def main():
    args = parse_args()
    data_path = Path(args.data_file)
    aggregate(str(data_path))

if __name__ == '__main__':
    main()
