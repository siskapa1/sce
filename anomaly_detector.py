#!/usr/bin/env python3
#
#   Copyright 2020 Pavel Siska <siskapa1@fit.cvut.cz>
#

import argparse
import datetime
import json
import logging
from pathlib import Path
import array as arr
import pandas
import matplotlib.pyplot as plt

IP_PROTO_TRANS_TABLE = {
    6: 'TCP',
    17: 'UDP',
}

def load_forecast(filename):
    data = pandas.read_json(filename)
    return data

def load_real_profile(filename):
    data = pandas.read_csv(
        filename,
        parse_dates=['time TIME_FIRST'],
        infer_datetime_format=True,
        dtype={
            'uint64 BYTES': 'uint64',
            'uint32 COUNT': 'uint32',
            'uint32 PACKETS': 'uint32',
            'uint8 PROTOCOL': 'uint8'
        })
    data.rename(
        columns={
            'time TIME_FIRST': 'time_first',
            'uint64 BYTES': 'bytes',
            'uint32 COUNT': 'flow_count',
            'uint32 PACKETS': 'packets',
            'uint8 PROTOCOL': 'protocol'
        },
        inplace=True)
    return data

def make_graph(yhat, yhat_lower, yhat_upper, real_data, timestamps, name, metric):
    ts = []
    for time in timestamps:
        ts.append(time.strftime("%d-%m-%Y (%H:%M:%S)"))

    x = ts 
    y = real_data

    plt.plot(x, y, color='green', label="reality")
    plt.plot(x, yhat, color='orange', label="yhat")
    plt.plot(x, yhat_lower, color='blue', label="yhat_lower")
    plt.plot(x, yhat_upper, color='red', label="yhat_upper")

    plt.xticks(rotation=90)
    plt.xlabel('Time [t]')
    plt.ylabel('{} [{}/hour]'.format(metric, metric))
    plt.gcf().subplots_adjust(bottom=0.55)
    plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')
    plt.tight_layout()

    plt.show()
    plt.savefig('{}-{}.png'.format(name, metric))
    plt.cla()
    plt.clf()

def detect_anomaly(anomaly_size, forecast, real_profile):
    yhat = []
    yhat_lower = []
    yhat_upper = []
    real_data = []
    timestamps = []

    for ip_proto_num, name in IP_PROTO_TRANS_TABLE.items():
        filtered_real = real_profile.loc[(real_profile['protocol'] == ip_proto_num)]
        for metric in ['bytes', 'packets', 'flow_count']: 
            filtered_forecast = forecast.loc[metric , name , ]

            time_window = 0
            is_tw_set = False

            y = 0
            y_lower = 0
            y_upper = 0
            real_sum = 0

            for row in filtered_forecast:
                time = datetime.datetime.strptime(row, '%Y-%m-%dT%H:%M:%S.%f')
                if is_tw_set == False:
                    time_window = time + datetime.timedelta(0,3600)
                    timestamps.append(time)
                    is_tw_set = True
                if time < time_window:
                    y = y + filtered_forecast[row]['yhat']
                    y_lower = y_lower + filtered_forecast[row]['yhat_lower']
                    y_upper = y_upper + filtered_forecast[row]['yhat_upper']
                else:
                    yhat.append(y)
                    yhat_lower.append(y_lower)
                    yhat_upper.append(y_upper)
                    timestamps.append(time)
                    time_window = time + datetime.timedelta(0,3600)
                    y = filtered_forecast[row]['yhat']
                    y_lower = filtered_forecast[row]['yhat_lower']
                    y_upper = filtered_forecast[row]['yhat_upper']

            yhat.append(y)
            yhat_lower.append(y_lower)
            yhat_upper.append(y_upper)

            time_window = 0
            is_tw_set = False

            for index, row in filtered_real.iterrows():
                if is_tw_set == False:
                    time_window = row['time_first'] + datetime.timedelta(0,3600)
                    is_tw_set = True
                if row['time_first'] < time_window:
                    real_sum = real_sum + row[metric]
                else:
                    real_data.append(real_sum)
                    time_window = row['time_first'] + datetime.timedelta(0,3600)
                    real_sum = row[metric]
    
            real_data.append(real_sum)

            if len(yhat) < len(real_data):
                diff = len(real_data) - len(yhat)
                real_data = real_data[:-diff]
            elif len(yhat) > len(real_data):
                diff = len(yhat) - len(real_data)
                yhat = yhat[:-diff]
                yhat_lower = yhat_lower[:-diff]
                yhat_upper = yhat_upper[:-diff]

            for idx, upper_limit in enumerate(yhat_upper):
                if real_data[idx] >= (upper_limit * float(anomaly_size)):
                    print("Anomaly found in {}-{} [{} - {}]: Real traffic is {} times bigger than maximal predicted value.".format(
                        name, metric, timestamps[idx], timestamps[idx] + datetime.timedelta(0,3600), round(real_data[idx] / (upper_limit), 2)))

            make_graph(yhat, yhat_lower, yhat_upper, real_data, timestamps, name, metric)
            yhat.clear()
            yhat_lower.clear()
            yhat_upper.clear()
            real_data.clear()
            timestamps.clear()

def parse_args():
    """Parse arguments and return parsed options."""
    parser = argparse.ArgumentParser(
        description='Detect anomaly in real and predicted network data.'
    )
    parser.add_argument('prediction', help='Path to file with predicted network data')
    parser.add_argument('reality', help='Path to file with real network data')
    parser.add_argument('threshold', help='Anomaly threshold')
    return parser.parse_args()

def main():
    args = parse_args()
    forecast = load_forecast(args.prediction)
    real = load_real_profile(args.reality)
    detect_anomaly(args.threshold, forecast, real)

if __name__ == '__main__':
    main()
