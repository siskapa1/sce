#!/usr/bin/env python3
#
#   Copyright 2020 Pavel Siska <siskapa1@fit.cvut.cz>
#

import argparse
import datetime
import json
import logging
from pathlib import Path
import array as arr
import pandas
import fbprophet

IP_PROTO_TRANS_TABLE = {
    6: 'TCP',
    17: 'UDP',
}

PROPHET_CHANGEPOINT_PRIOR_SCALE = 0.01

PROPHET_PREDICT_PERIODS = 48

def load_data(data_file_path):
    data = pandas.read_csv(
        data_file_path,
        parse_dates=['time TIME_FIRST'],
        infer_datetime_format=True,
        dtype={
            'uint64 BYTES': 'uint64',
            'uint32 COUNT': 'uint32',
            'uint32 PACKETS': 'uint32',
            'uint8 PROTOCOL': 'uint8'
        })
    data.rename(
        columns={
            'time TIME_FIRST': 'time_first',
            'uint64 BYTES': 'bytes',
            'uint32 COUNT': 'flow_count',
            'uint32 PACKETS': 'packets',
            'uint8 PROTOCOL': 'protocol'
        },
        inplace=True)
    return data

def make_forecast(data):
    model = fbprophet.Prophet(changepoint_prior_scale=PROPHET_CHANGEPOINT_PRIOR_SCALE, 
                              seasonality_mode='multiplicative', interval_width=0.95).fit(data)
    future = model.make_future_dataframe(periods=PROPHET_PREDICT_PERIODS,
                                         freq='5min', include_history=False)
    forecast = model.predict(future)
    return forecast.set_index('ds')[['yhat', 'yhat_lower', 'yhat_upper']].to_dict('index')

def create_file(profile):
    profile_json = json.dumps(profile)
    profile_path = Path('profile-{}.json'.format(datetime.datetime.now().isoformat()))
    profile_path.write_text(profile_json)
    latest_link_path = Path('latest.json.new')
    latest_link_path.symlink_to(profile_path.name)
    latest_link_path.replace('latest.json')

def make_profiles(data_file_path):
    forecast = dict()
    data = load_data(data_file_path)

    for ip_proto_num, name in IP_PROTO_TRANS_TABLE.items():
        # Filter data by protocol number
        filtered_data = data.loc[(data['protocol'] == ip_proto_num)]
        if filtered_data.empty == True:
            continue

        forecast[name] = dict()
            
        for metric in ['bytes', 'packets', 'flow_count']: 
            # Prepare data for FBProphet expected format
            proto_data_prophet = filtered_data[['time_first', metric]].rename(columns={
                'time_first': 'ds',
                metric: 'y'
                })
            # Drop top 1% values (outliers)
            proto_data_prophet_95th = proto_data_prophet.loc[
                proto_data_prophet['y'] <= proto_data_prophet['y'].quantile(0.99)]
            forecast[name][metric] = {
                k.isoformat(): v
                for k, v in make_forecast(proto_data_prophet_95th).items()
                }
    create_file(forecast)

def parse_args():
    """Parse arguments and return parsed options."""
    parser = argparse.ArgumentParser(
        description='Generate "prediction profile" based on aggregated historical network data.'
    )
    parser.add_argument('data_file', help='Path to data file')
    return parser.parse_args()

def main():
    args = parse_args()
    data_path = Path(args.data_file)
    make_profiles(str(data_path))

if __name__ == '__main__':
    main()
